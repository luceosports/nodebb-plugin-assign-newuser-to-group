<div class="row">
    <div class="col-lg-9">
        <div class="panel panel-default">
            <div class="panel-heading"><i class="fa fa-check"></i> Assign new user to group</div>
            <div class="panel-body">
                <p>Enter a group name</p>
                <form role="form" class="assign-newuser-to-group-settings" >
                    <div class="form-group col-xs-6">
						<label for="userGroup">Group Name</label>
						<input type="text" id="userGroup" name="userGroup" title="Group Name" class="form-control" placeholder="group name">
					</div>
                    <div class="form-group col-xs-12">
                        <label for="userGroup">User Attributes Extended</label>
                        <ol style="padding: 0;">
                            <li class="js-sample js-user-group-ext-li" style="display:none;">
                                <div class="form-group col-xs-3">
                                    <input disabled="disabled" type="text" name="attr_name" title="User Attr Name" class="form-control" placeholder="user attr name">
                                </div>
                                <div class="form-group col-xs-3">
                                    <input disabled="disabled" type="text" name="attr_value" title="User Attr Value" class="form-control" placeholder="user attr value">
                                </div>
                                <div class="form-group col-xs-5">
                                    <input disabled="disabled" type="text" name="group" title="Group Name" class="form-control" placeholder="group name">
                                </div>
                                <span class="js-remove-field" style="color: red; cursor: pointer;">X</span>
                                <div style="clear: both;"></div>
                            </li>
                        </ol>
                        <button class="js-user-group-extended-add">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
		<div class="panel panel-default">
			<div class="panel-heading">Control Panel</div>
			<div class="panel-body">
				<button class="btn btn-primary" id="save">Save Settings</button>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
	require(['settings'], function(Settings) {
        Settings.load('assign-newuser-to-group', $('.assign-newuser-to-group-settings'), function (err, values) {
	        let settingsData = ajaxify.data.settings;
            if (settingsData.userGroupExtended) {
            	let userGroupExtended = JSON.parse(decodeURIComponent(settingsData.userGroupExtended))
            	for (let item of userGroupExtended) {
		            clone_sample(item);
	            }
            }
        });

        $('#save').on('click', function() {
            let formElement = $('.assign-newuser-to-group-settings')

            let userGroupExtData = [];
	        formElement.find('.js-user-group-ext-li').not(".js-sample").each(function() {
	        	let liData = new Object();
	        	$(this).find('input').each(function (index, elem) {
			        let $elem = $(elem);
			        liData[$elem.attr('name')] = $elem.val();
			        $elem.attr('disabled', 'disabled');
		        })
                userGroupExtData.push(liData)
	        });

	        formElement.find('input[name="userGroupExtended"]').remove()
	        formElement.append('<input type="hidden" name="userGroupExtended" value="' + encodeURIComponent(JSON.stringify(userGroupExtData)) + '" />')

            Settings.save('assign-newuser-to-group', formElement, function() {
                app.alert({
                    type: 'success',
                    alert_id: 'assign-newuser-to-group-saved',
                    title: 'Settings Saved',
                    message: 'Click here to reload NodeBB',
                    timeout: 2500,
                    clickfn: function() {
                        socket.emit('admin.reload');
                    }
                });
            });
        });

        $('.js-user-group-extended-add').click(function () {
	        clone_sample();
            return false;
        })

        $('body').on('click', '.js-remove-field', function () {
	        if(confirm('Remove?')) {
		        $(this).closest('li').remove();
	        }
	        return false;
        })
    });

	function clone_sample(data) {
		data = data || new Object();

		let $sample = $('.js-sample');
		let $newElem = $sample.clone().removeClass('js-sample');
		$newElem.find('input,select,textarea').removeAttr('disabled');
		$newElem.find('input,select,textarea').each(function(index, elem) {
			let $elem = $(elem);
			let $name = $elem.attr('name');
			if ($name) {
				let newVal = ''
                if (data[$name]) {
	                newVal = data[$name]
                }
				$elem.val(newVal)
			}
		});

		$newElem.insertBefore($sample).show();
	}



</script>