var	fs = require('fs'),
    groups = require.main.require('./src/groups'),
	winston = require.main.require('winston'),
	Meta = require.main.require('./src/meta'),
	User = require.main.require('./src/user'),

	AssignNewUser = {},
    userGroup = null,
	userGroupExtended = [];

AssignNewUser.init = function(params, callback) {
	function render(req, res, next) {
		res.render('admin/plugins/assign-newuser-to-group', {});
	}

    Meta.settings.get('assign-newuser-to-group', function(err, settings) {
		if (!err) {
			if (settings && settings.userGroupExtended) {
				userGroupExtended = JSON.parse(decodeURIComponent(settings.userGroupExtended))
			}
			if (settings && settings.userGroup) {
				userGroup = settings.userGroup;
			} else {
				winston.error('[plugins/assign-newuser-to-group] User group not set!');
			}
		}
	});

	params.router.get('/admin/plugins/assign-newuser-to-group', params.middleware.admin.buildHeader, render);
	params.router.get('/api/admin/plugins/assign-newuser-to-group', render);

	callback();
};

AssignNewUser.assignUserToGroup = function(hookData) {
    if (hookData && hookData.user) {
    	if (userGroup != null) {
		    console.log('Joining user to main group: ' + userGroup)
		    groups.join(userGroup, hookData.user.uid);
	    }
	    AssignNewUser.joinUserBasedOnExtendedAttrs(hookData.user.uid, hookData.user)
    }
};

AssignNewUser.assignUserToGroupBasedOnCustomFields = function(data) {
	if (data.type === 'set' && data.field === 'luceoExtra') {
		User.getUserField(data.uid, data.field, function (error, extraUserData) {
			AssignNewUser.joinUserBasedOnExtendedAttrs(data.uid, extraUserData)
		});
	}
}

AssignNewUser.joinUserBasedOnExtendedAttrs = function(uid, userData) {
	if (userGroupExtended.length) {
		for (let extItem of userGroupExtended) {
			if (typeof extItem.group !== "undefined" && typeof userData[extItem.attr_name] !== "undefined") {
				let match = false;
				if (Array.isArray(userData[extItem.attr_name])) {
					match = userData[extItem.attr_name].findIndex(el => el == extItem.attr_value) !== -1
				} else {
					match = userData[extItem.attr_name] == extItem.attr_value
				}
				if (match) {
					console.log('Joining user to group: ' + extItem.group)
					groups.join(extItem.group, uid);
				}
			}
		}
	}
}

AssignNewUser.admin = {
	menu: function(custom_header, callback) {
		custom_header.plugins.push({
			"route": '/plugins/assign-newuser-to-group',
			"icon": 'fa-check',
			"name": 'Assign new user to group'
		});

		callback(null, custom_header);
	}
};

module.exports = AssignNewUser;
